import React from 'react';
import './error-notify.css';
import './__text/error-notify_text.css';

const ErrorNotify = ({ text= 'Ошибка' })=> {
  return(
    <div className="error-notify">
      <p className="error-notify_text">{text}</p>
    </div>
  )
};

export default ErrorNotify;
