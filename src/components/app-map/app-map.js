import React, {Component} from 'react';
import { YMaps, Map, Placemark, Polyline } from 'react-yandex-maps';
import Spinner from '../spinner';
import ErrorNotify from '../error-notify';
import './app-map.css';

export default class AppMap extends Component {
  
  state = {
    loading: true,
    hasError: false
  };
   
  render() {
    if (this.state.hasError) {
      return <ErrorNotify text='Ошибка при загрузке карты.' />
    }
    
    const { points, mapConfig, onPointMoved, onMapMoved } = this.props;
    const { loading } = this.state;
    
    const placemarkList = points.map((item)=> {
      return (
          <Placemark
              key= {item.id}
              modules= {['geoObject.addon.balloon']}
              geometry= {item.coords}
              properties= {{
                balloonContentHeader: item.label,
              }}
              options= {{
                draggable: true,
                preset: 'islands#violetDotIcon'
              }}
              onDragend= {(event) => {
                onPointMoved(item.id, event.get('target').geometry.getCoordinates());
              }}
            />
      )
    });
  
    const lineCoords = points.map((item) => {
      return item.coords;
    });
    
    const polyline = (
        <Polyline
            geometry= {lineCoords}
            options= {{
              balloonCloseButton: false,
              strokeColor: '#5a009d',
              strokeWidth: 3,
              strokeOpacity: 0.4,
            }}
        />
    );
    
    const spinner = loading ? <Spinner/> : null;
    
    return(
        <div className="app-map">
          {spinner}
          
          <YMaps>
            <Map
                state= {mapConfig}
                width= "100%"
                height= "100%"
                modules= {['control.ZoomControl']}
                
                onLoad= {() => {
                  this.setState({loading: false})
                }}
                
                onError= {() => {
                  this.setState({hasError: true})
                }}
                
                onBoundsChange= {(event) => {
                  onMapMoved(event.get('map').getCenter());
                }}
                
                onSizechange= {(event) => {
                  event.get('map').container.fitToViewport();
                }}>
              {placemarkList}
              {polyline}
            </Map>
          </YMaps>
        </div>
    )
  };
};
