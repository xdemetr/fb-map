import React, {Component} from 'react';
import AppMap from '../app-map';
import PointList from '../point-list';
import PointAddForm from '../point-add-form';
import './app.css';
import './__aside/app__aside.css';
import './__map/app__map.css';

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

let defaultMapConfig = {
  center: [55.79, 49.12],
  zoom: 14,
  controls: ['zoomControl']
};

export default class App extends Component {
  startId = 1;
  
  state = {
    mapConfig: defaultMapConfig,
    pointsData: [
      this.createPoint('Первая точка', [ 55.79757220209139, 49.10626419067258 ]),
      this.createPoint('Вторая', [ 55.79002844880397, 49.12876342773091 ]),
      this.createPoint('Третья точка', [ 55.7809111510745, 49.12554291534413 ]),
    ],
  };
  
  createPoint(label, coords= this.state.mapConfig.center) {
    return {
      id: this.startId++,
      label,
      coords
    }
  }
  
  addPoint = (label) => {
    this.setState (({pointsData}) => {
      const newItem = this.createPoint(label);
      const newArray = [
        ...pointsData,
        newItem
      ];
      return {
        pointsData: newArray
      };
    });
  };
  
  deletePoint = (id) => {
    this.setState(({pointsData}) => {
      const idx = pointsData.findIndex((el) => el.id === id);
      
      const newArray = [
        ...pointsData.slice(0, idx),
        ...pointsData.slice(idx+1)
      ];
    
      return {
        pointsData: newArray
      }
    });
  };
  
  onPointMoved = (id, coords) => {
    const { pointsData } = this.state;

    const idx = pointsData.findIndex((el) => el.id === id);
    const oldItem = pointsData[idx];
    const newItem = {...oldItem, coords: coords};

    const newArray= [
      ...pointsData.slice(0, idx),
      newItem,
      ...pointsData.slice(idx+1)
    ];
  
    this.setState(() => {
      return {
        pointsData: newArray
      }
    });
  };
  
  onMapMoved = (coords) => {
    defaultMapConfig.center = coords;
    
    this.setState(() => {
      return{
        mapConfig: defaultMapConfig
      }
    });
  };
  
  onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    
    const pointsData = reorder(
        this.state.pointsData,
        result.source.index,
        result.destination.index
    );
    
    this.setState({
      pointsData
    });
  };
  
  render(){
    const { pointsData, mapConfig } = this.state;
        
    return(
        <div className="app">
          <main className="app__map">
            <AppMap
                points= {pointsData}
                mapConfig= {mapConfig}
                onPointMoved= {this.onPointMoved}
                onMapMoved= {this.onMapMoved} />
          </main>
          <aside className="app__aside">
            <PointAddForm onPointAdded= {this.addPoint} />
            
            <PointList
                points= {pointsData}
                onDeleted= {this.deletePoint}
                onDragEnd= {this.onDragEnd} />
          </aside>
        </div>
    )
  }
}
