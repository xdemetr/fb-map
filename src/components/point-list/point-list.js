import React from 'react';
import PointListItem from '../point-list-item';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import './point-list.css';
import './__item/point-list__item.css';
import './_empty/point-list_empty.css';

const getItemStyle = (isDragging, draggableStyle) => ({
  background: isDragging ? '#eee' : '',
  ...draggableStyle,
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? '' : ''
});

const PointList = ({ points, onDeleted, onDragEnd }) => {
  
  const items = points.map((item, index) => (
      <Draggable key={item.id} draggableId={item.id} index={index}>
        {(provided, snapshot) => (
            <li
                className="point-list__item"
                ref= {provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
                style= {getItemStyle(
                    snapshot.isDragging,
                    provided.draggableProps.style
                )}>
              <PointListItem
                  item= {item}
                  onDeleted = {() => onDeleted(item.id)} />
            </li>
          )}
        </Draggable>
    ));
  
  
    const content = (items.length > 0) ? items : null;
    const empty = <div className="point-list_empty">Нет точек для построения маршрута.</div>;
  
    if(!content)
      return empty;
  
    return(
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
                <ul
                    ref= {provided.innerRef}
                    style= {getListStyle(snapshot.isDraggingOver)}
                    className="point-list">
                  {content}
                  {provided.placeholder}
                </ul>
            )}
          </Droppable>
        </DragDropContext>
    )
};

export default PointList;
