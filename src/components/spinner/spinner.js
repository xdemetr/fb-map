import React from 'react';
import './spinner.css';

const Spinner = () => {
  return(
      <div className="spinner">
        <div className="spinner__lds">
          <div className="spinner__dual-ring">
          </div>
        </div>
      </div>
  )
};

export default Spinner;
