import React from 'react';
import './point-list-item.css';
import './__label/point-list-item__label.css';
import './__between-text/point-list-item__between-text.css';
import '../button';

const PointListItem = ({ item, onDeleted }) => {
  
  const { label } = item;
  
  return(
      <div className="point-list-item">
        <span className="point-list-item__label">{label}</span>
        <span className="point-list-item__between-text">&nbsp;</span>
        <span className="point-list-item__actions">
          <span
              className="button button_only-icon button_action_delete"
              onClick={onDeleted}>Удалить</span>
        </span>
      </div>
  )
};

export default PointListItem;
