import React, {Component} from 'react';
import './point-add-form.css';
import './__input/point-add-form__input.css';

export default class PointAddForm extends Component {
  
  state = {
    label: ''
  };
  
  onLabelChange = (event) => {
    this.setState ({
      label: event.target.value
    })
  };
  
  onSubmit = (event) => {
    event.preventDefault();
    if (this.state.label) {
      this.props.onPointAdded(this.state.label);
    }
    this.setState({
      label: ''
    })
  };
  
  render(){
    return(
        <form className="point-add-form" onSubmit={this.onSubmit}>
          <div className="point-add-form__control">
            <input
                type="text"
                className="point-add-form__input"
                placeholder="Новая точка"
                value= {this.state.label}
                onChange= {this.onLabelChange} />
          </div>
        </form>
    )
  }
};
